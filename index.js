// start BT1
/**
 * INPUT:
 * - Ba số ngẫu nhiên
 * CÁC BƯỚC XỬ LÝ:
 * - Gắn giá trị num1, num2, num3 lần lượt từ input;
 * - So sánh các giá trị tìm min, max, mid;
 * - Xuất ra min, mid, max.
 * OUTPUT:
 * - Thứ tự tăng dần của ba số
 */
function sortUp() {
    var num1 = document.getElementById('num1').value * 1;
    var num2 = document.getElementById('num2').value * 1;
    var num3 = document.getElementById('num3').value * 1;

    var min;
    var mid;
    var max;
    if (num1 >= num2 && num1 >= num3) {
        max = num1;
        if (num2 >= num3) {
            mid = num2;
            min = num3;
            document.getElementById('sortUp').innerHTML = ` Thứ tự tăng dần: ${min}, ${mid}, ${max}`;
        }
        else {
            mid = num3;
            min = num2;
            document.getElementById('sortUp').innerHTML = ` Thứ tự tăng dần: ${min}, ${mid}, ${max}`;

        }
    } else if (num2 >= num1 && num2 >= num3) {
        max = num2;
        if (num1 >= num3) {
            mid = num1;
            min = num3;
            document.getElementById('sortUp').innerHTML = ` Thứ tự tăng dần: ${min}, ${mid}, ${max}`;

        }
        else {
            mid = num3;
            min = num1;
            document.getElementById('sortUp').innerHTML = ` Thứ tự tăng dần: ${min}, ${mid}, ${max}`;
        }
    } else {
        max = num3;
        if (num1 >= num2) {
            mid = num1;
            min = num2;
            document.getElementById('sortUp').innerHTML = ` Thứ tự tăng dần: ${min}, ${mid}, ${max}`;

        }
        else {
            mid = num2;
            min = num1;
            document.getElementById('sortUp').innerHTML = ` Thứ tự tăng dần: ${min}, ${mid}, ${max}`;

        }
    }
}
// end BT1

// start BT2
/**
 * INPUT:
 * - 4 options
 * CÁC BƯỚC XỬ LÝ:
 * - Gắn giá trị rightPerson ở js từ file html;
 * - Dùng switch case cho từng trường hợp và đưa ra lời chào;
 * OUTPUT:
 * - Lời chào theo từng người.
 */
function sayHello() {
    const rightPerson = document.getElementById('rightPerson').value;
    switch (rightPerson) {
        case "B":
            document.getElementById('sayHello').innerHTML = ` Chào Bố! Bố vẫn khỏe chứ? <br /> Các ông bố hãy dành nhiều thời gian cho con, chơi với con, đùa nghịch với con, đọc truyện cho con, nói chuyện với con và cùng con làm những việc đơn giản. Ngoài việc gắn kết tình cảm cha con, còn giúp con phát triển toàn diện cả về thể chất lẫn tinh thần.`;
            break;
        case "M":
            document.getElementById('sayHello').innerHTML = ` Chào Mẹ! Mẹ vẫn khỏe chứ? <br /> Chúc mẹ có luôn vui vẻ và tươi trẻ mẹ nhé! `;
            break;
        case "A":
            document.getElementById('sayHello').innerHTML = ` Ái chà chà, lại là Con trai cưng của gia đình đây rồi! Anh vẫn khỏe chứ? <br /> Hơn 20 tuổi đầu, khi ở nhà mình đừng làm em bé nữa nghen.`;
            break;
        case "E":
            document.getElementById('sayHello').innerHTML = ` Ái chà chà, lại là Con gái rượu của gia đình đây rồi! Em vẫn khỏe chứ? <br /> Bản thân con gái rượu thì chỉ gặp con trai cưng. Chúc em bé một đời như ý nhóe! <3`;
            break;
    }
}
// end BT2

// start BT3
/**
 * INPUT:
 * - 3 số nguyên.
 * CÁC BƯỚC XỬ LÝ:
 * - Gắn giá trị cho từng số num1, num2, num3;
 * - Áp dụng hàm Number.isInteger(value) kiểm tra xem có phải số nguyên không;
 *      -> không phải -> đưa ra cảnh báo;
 *      -> phải -> xét các trường hợp;
 * OUTPUT:
 * - Bao nhiêu số chẵn, bao nhiêu số lẻ.
 */
function evenOdd() {
    var num1 = document.getElementById('numN1').value * 1;
    console.log("🚀 ~ file: index.js:88 ~ evenOdd ~ num1", num1)
    var num2 = document.getElementById('numN2').value * 1;
    console.log("🚀 ~ file: index.js:90 ~ evenOdd ~ num2", num2)
    var num3 = document.getElementById('numN3').value * 1;
    console.log("🚀 ~ file: index.js:92 ~ evenOdd ~ num3", num3)

    if (Number.isInteger(num1) && Number.isInteger(num2) && Number.isInteger(num3)) {
        if (num1 % 2 == 0 && num2 % 2 == 0 && num3 % 2 == 0) {
            document.getElementById('evenOdd').innerHTML = ` Cả ba số đều là số chẵn.`
        } else if ((num1 % 2 == 0 && num2 % 2 == 0 && num3 % 2 != 0) ||
            (num1 % 2 == 0 && num2 % 2 != 0 && num3 % 2 == 0) ||
            (num1 % 2 != 0 && num2 % 2 == 0 && num3 % 2 == 0)) {
            document.getElementById('evenOdd').innerHTML = ` Có hai số chẵn và một số lẻ.`
        } else if ((num1 % 2 != 0 && num2 % 2 == 0 && num3 % 2 != 0) ||
            (num1 % 2 != 0 && num2 % 2 != 0 && num3 % 2 == 0) ||
            (num1 % 2 == 0 && num2 % 2 != 0 && num3 % 2 != 0)) {
            document.getElementById('evenOdd').innerHTML = ` Có một số chẵn và hai số lẻ.`
        } else {
            document.getElementById('evenOdd').innerHTML = ` Cả ba số đều là số lẻ`
        }
    }
    else {
        alert('Vui lòng nhập số nguyên!!!')
    }
}
// end BT3

// start BT4
/**
 * INPUT:
 * - 3 cạnh tam giác.
 * CÁC BƯỚC XỬ LÝ:
 * - Gắn giá trị cho từng cạnh edge1, edge2, edge3;
 * - Kiểm tra tính tồn tại của tam giác:
 *      (1) Tổng hai cạnh không được nhỏ hơn cạnh còn lại. 
        (2) Giá trị các cạnh phải là số dương
 *      -> không thỏa -> nhập lại;
        -> thỏa mãn -> xét các trường hợp;
 * OUTPUT:
 * - Loại tam giác.
 */
function defineTriangle() {
    var edge1 = document.getElementById('edge1').value * 1;
    var edge2 = document.getElementById('edge2').value * 1;
    var edge3 = document.getElementById('edge3').value * 1;
    if ((edge1 >= edge2 + edge3)
        || (edge2 >= edge1 + edge3)
        || (edge3 >= edge1 + edge2)
        || (edge1 == 0)
        || (edge2 == 0)
        || (edge3 == 0)) {
        alert(`Không đủ điều kiện hình thành một tam giác
            (1) Tổng hai cạnh không được nhỏ hơn cạnh còn lại. 
            (2) Giá trị các cạnh phải là số dương
                --> Vui lòng nhập lại giá trị mới`)
    } else {
        if ((edge1 == edge2)
            && (edge1 == edge3)) {
            document.getElementById('defineTriangle').innerHTML = `Tam giác đều`
        } else if ((edge1 == edge2)
            || (edge1 == edge3)
            || (edge2 == edge3)) {
            document.getElementById('defineTriangle').innerHTML = `Tam giác cân`
        } else if ((Math.pow(edge1, 2) == Math.pow(edge2, 2) + Math.pow(edge3, 2))
            || (Math.pow(edge2, 2) == Math.pow(edge1, 2) + Math.pow(edge3, 2))
            || (Math.pow(edge3, 2) == Math.pow(edge1, 2) + Math.pow(edge2, 2))) {
            document.getElementById('defineTriangle').innerHTML = `Tam giác vuông`
        } else {
            document.getElementById('defineTriangle').innerHTML = `Tam giác thường`
        }
    }
}
// end BT4

// start BT5
/**
 * INPUT:
 * - 3 giá trị: ngày,tháng, năm.
 * CÁC BƯỚC XỬ LÝ:
 * - Gắn giá trị ngày, tháng, năm cho từng biến dayValue, monthValue, yearValue;
 * - Xét ngày, tháng, năm có hợp lệ không?
 * - Xét từng trường hợp đặc biệt của tháng 1, 2, 3, 8 khi tìm ngày hôm qua;
 * - Xét từng trường hợp đặc biệt của tháng 2, 12 khi tìm ngày mai;
 * - các trường hợp 30 ngày và 31 ngày cố định khác gộp chung;
 ** OUTPUT:
 * - Ngày trước và sau.
 */
function namNhuan(year){
    if (year % 4 == 0 && year % 100 != 0) {
        return `N`;
    } else if (year % 4 == 0 && year % 100 == 0 && year % 400 == 0) {
        return `N`;
    } else {
        return `KN`;
    }
}
function thang1HomQua(day, year) {
    if (day == 1) {
        return `Ngày hôm qua là 31/12/${year - 1}`;
    } else {
        return `Ngày hôm qua là ${day - 1}/01/${year}`;
    }
}
function thang2HomQua(day, year) {
    if (namNhuan(year)== `KN` && day>28){
        return alert(` Tháng 2 năm ${year} không có quá 28 ngày. Nhập lại ngày tối đa 28.`)
    } else if (namNhuan(year)==`N` && day>29){
        return alert(` Tháng 2 năm ${year} không có quá 29 ngày. Nhập lại ngày tối đa 29.`)
    } else if (day == 1){
        return ` Ngày hôm qua là 31/01/${year}`
    } else{
        return `Ngày hôm qua là ${day-1}/02/${year}`;
    }
}
function thang3HomQua(day, year) {
    if (day>30){
        return alert(`Vui lòng nhập ngày tối đa là 30`)
    } else if (day == 1 && namNhuan(year) == 'N') {
        return `Ngày hôm qua là ngày 29/02/${year}`
    } else if (day == 1 && namNhuan(year) == 'KN') {
        return `Ngày hôm qua là ngày 28/02/${year}`
    } else {
        return `Ngày hôm qua là ${day - 1}/03/${year}`;
    }
}
function thang31NgayHomQua(day, month, year) {
    if (day==1 && month==8){
        return `Ngày hôm qua là 31/07/${year}`
    } else if (day == 1) {
        return `Ngày hôm qua là 30/${month - 1}/${year}`
    } else {
        return `Ngày hôm qua là ${day - 1}/${month}/${year}`
    }
}
function thang30NgayHomQua(day, month, year) {
    if (day == 31) {
        return alert(`Tháng ${month} không có ngày 31. Vui lòng nhập tối đa là ngày 30!`)
    } else if (day == 1) {
        return `Ngày hôm qua là 31/${month - 1}/${year}`
    } else {
        return `Ngày hôm qua là ${day - 1}/${month}/${year}`
    }
}
function lastDay() {
    var dayValue = document.getElementById('dayValue').value * 1;
    var monthValue = document.getElementById('monthValue').value * 1;
    var yearValue = document.getElementById('yearValue').value * 1;
    if (dayValue < 1 || dayValue > 31 || monthValue > 12 || monthValue < 1 || yearValue < 1 || Number.isInteger(monthValue) == false || Number.isInteger(yearValue) == false || Number.isInteger(dayValue) == false) {
        alert(` Vui lòng nhập dữ liệu hợp lệ`);
    } else if (monthValue == 1) {
        document.getElementById('dayResult').innerHTML = thang1HomQua(dayValue, yearValue);
    } else if (monthValue == 3) {
        document.getElementById('dayResult').innerHTML = thang3HomQua(dayValue, yearValue);
    } else if (monthValue == 2) {
        document.getElementById('dayResult').innerHTML = thang2HomQua(dayValue, yearValue);
    } else if (monthValue == 5
        || monthValue == 7
        || monthValue == 8
        || monthValue == 10
        || monthValue == 12) {
        document.getElementById('dayResult').innerHTML = thang31NgayHomQua(dayValue, monthValue, yearValue);
    } else {
        document.getElementById('dayResult').innerHTML = thang30NgayHomQua(dayValue, monthValue, yearValue);
    }
}

function thang2NgayMai(day, year) {
    if (namNhuan(year)== `KN` && day>28){
        return alert(` Tháng 2 năm ${year} không có quá 28 ngày. Nhập lại ngày tối đa 28.`)
    } else if (namNhuan(year)==`N` && day>29){
        return alert(` Tháng 2 năm ${year} không có quá 29 ngày. Nhập lại ngày tối đa 29.`)
    } else if ((namNhuan(year) == 'N' && day == 29)
        || (namNhuan(year) == 'KN' && day == 28)) {
        return ` Ngày mai là 01/03/${year}`
    } else {
        return ` Ngày mai là ${day + 1}/02/${year}`
    }
}
function thang12NgayMai(day, year) {
    if (day == 31) {
        return ` Ngày mai là 01/01/${year + 1}`
    } else {
        return ` Ngày mai là ${day + 1}/12/${year}`
    }
}
function thang31NgayNgayMai(day, month, year) {
    if (day == 31) {
        return `Ngày mai là 01/${month + 1}/${year}`
    } else {
        return `Ngày mai là ${day+1}/${month}/${year}`
    }
}
function thang30NgayNgayMai(day, month, year) {
    if (day == 31) {
        return alert(` Tháng ${month} không có ngày 31. Vui lòng nhập tối đa ngày 30!`)
    } else if (day == 30) {
        return `Ngày mai là 01/${month + 1}/${year}`
    } else {
        return `Ngày mai qua là ${day + 1}/${month}/${year}`
    }
}
function nextDay() {
    var dayValue = document.getElementById('dayValue').value * 1;
    var monthValue = document.getElementById('monthValue').value * 1;
    var yearValue = document.getElementById('yearValue').value * 1;
    if (dayValue < 1 || dayValue > 31 || monthValue > 12 || monthValue < 1 || yearValue < 1 || Number.isInteger(monthValue) == false || Number.isInteger(yearValue) == false || Number.isInteger(dayValue) == false) {
        alert(` Vui lòng nhập dữ liệu hợp lệ`);
    } else if (monthValue == 2) {
        document.getElementById('dayResult').innerHTML = thang2NgayMai(dayValue, yearValue);
    } else if (monthValue == 12) {
        document.getElementById('dayResult').innerHTML = thang12NgayMai(dayValue, yearValue);
    } else if (monthValue == 1
        || monthValue == 3
        || monthValue == 5
        || monthValue == 7
        || monthValue == 8
        || monthValue == 10) {
        document.getElementById('dayResult').innerHTML = thang31NgayNgayMai(dayValue, monthValue, yearValue);
    } else {
        document.getElementById('dayResult').innerHTML = thang30NgayNgayMai(dayValue, monthValue, yearValue);
    }
}
// end BT5

// start BT6
/**
 * INPUT:
 * - 3 giá trị:tháng, năm.
 * CÁC BƯỚC XỬ LÝ:
 * - Gắn giá trị tháng, năm cho từng biến monthValue2, yearValue2;
 * - Xét tháng, năm có hợp lệ không?
 * - Xét năm nhuần và không nhuần để xét cho tháng 2;
 ** OUTPUT:
 * - Tháng có bao nhiêu ngày.
 */
function leapYear() {
    var monthValue2 = document.getElementById('monthValue2').value * 1;
    var yearValue2 = document.getElementById('yearValue2').value * 1;
    var result;

    if (yearValue2 % 4 == 0 && yearValue2 % 100 != 0) {
        result = 'N';
    } else if (yearValue2 % 4 == 0 && yearValue2 % 100 == 0 && yearValue2 % 400 == 0) {
        result = 'N';
    } else {
        result = 'KN';
    }

    if (monthValue2 > 12 || monthValue2 < 1 || yearValue2 < 1 || Number.isInteger(monthValue2) == false || Number.isInteger(yearValue2) == false) {
        alert(` Vui lòng nhập dữ liệu hợp lệ`);
    } else if (result == 'N' && monthValue2 == 2) {
        document.getElementById('leapYear').innerHTML = ` Tháng ${monthValue2} năm ${yearValue2} có 29 ngày`
    } else if (result == 'KN' && monthValue2 == 2) {
        document.getElementById('leapYear').innerHTML = ` Tháng ${monthValue2} năm ${yearValue2} có 28 ngày`
    } else if (monthValue2 == 1
        || monthValue2 == 3
        || monthValue2 == 5
        || monthValue2 == 7
        || monthValue2 == 8
        || monthValue2 == 10
        || monthValue2 == 12){
        document.getElementById('leapYear').innerHTML = ` Tháng ${monthValue2} năm ${yearValue2} có 31 ngày`
    } else {
        document.getElementById('leapYear').innerHTML = ` Tháng ${monthValue2} năm ${yearValue2} có 30 ngày`
    }
}
// end BT6

// start BT7
/**
 * INPUT:
 * - Số nguyên dương có 3 chữ số.
 * CÁC BƯỚC XỬ LÝ:
 * - Gắn giá trị số nguyên dương cho biến;
 * - Xét số có phải có 3 chữ số ( >=100 && <=999)và là số nguyên dương không?
 * - tìm các hàng đơn vị, trăm, chục -> áp dụng hàm để chuyển chữ thành số.
 * - xuất ra màn hình cách đọc
 ** OUTPUT:
 * - cách đọc.
 */
function doiChu(a) {
    switch (a) {
        case 1:
            return 'một';
        case 2:
            return 'hai';
        case 3:
            return 'ba';
        case 4:
            return 'bốn';
        case 5:
            return 'năm';
        case 6:
            return 'sáu';
        case 7:
            return 'bảy';
        case 8:
            return 'tám';
        case 9:
            return 'chín';
        case 0:
            return 'không';
    }
}
function readNumber() {
    var threeNumber = document.getElementById('threeNumber').value * 1;
    if (threeNumber > 99 && threeNumber < 1000 && Number.isInteger(threeNumber)) {
        var hangTram = Math.floor(threeNumber / 100);
        console.log("🚀 ~ file: index.js:234 ~ readNumber ~ hangTram", hangTram)
        var hangChuc = Math.floor((threeNumber % 100) / 10);
        console.log("🚀 ~ file: index.js:235 ~ readNumber ~ hangChuc", hangChuc)
        var donVi = (threeNumber % 100) % 10;
        console.log("🚀 ~ file: index.js:236 ~ readNumber ~ donVi", donVi)
        var hangTramChu = doiChu(hangTram);
        console.log("🚀 ~ file: index.js:237 ~ readNumber ~ hangTramChu", hangTramChu)
        var hangChucChu = doiChu(hangChuc);
        console.log("🚀 ~ file: index.js:239 ~ readNumber ~ hangChucChu", hangChucChu)
        var donViChu = doiChu(donVi);
        console.log("🚀 ~ file: index.js:241 ~ readNumber ~ donViChu", donViChu)

        if (hangChuc == 0 && donVi == 0) {
            document.getElementById('readNumber').innerHTML = `${hangTramChu} trăm`
        } else if (hangChuc == 0) {
            document.getElementById('readNumber').innerHTML = `${hangTramChu} trăm lẻ ${donViChu}`
        } else if (donVi == 0) {
            if (hangChuc == 1) {
                document.getElementById('readNumber').innerHTML = `${hangTramChu} trăm mười`
            } else {
                document.getElementById('readNumber').innerHTML = `${hangTramChu} trăm ${hangChucChu} mươi`
            }
        } else if (hangChuc == 1) {
            document.getElementById('readNumber').innerHTML = `${hangTramChu} trăm mười ${donViChu}`
        } else {
            document.getElementById('readNumber').innerHTML = `${hangTramChu} trăm ${hangChucChu} mươi ${donViChu}`
        }
    } else {
        alert('Số không hợp lệ')
    }
}
// end BT7

// start BT8
/**
 * INPUT:
 * - Tên các sinh viên và tọa độ.
 * - Tọa độ trường học.
 * CÁC BƯỚC XỬ LÝ:
 * - Gắn giá trị tên và các tọa độ cho biến;
 * - Tạo một hàm function để tính khoảng cách từ nhà sinh viên đến trường;
 * - So sánh khoảng cách -> tìm ra max
 ** OUTPUT:
 * - Sinh viên có nhà xa nhất.
 */
function distance(a1, a2, s1, s2) {
    return Math.sqrt(Math.pow(s1 - a1, 2) + Math.pow(s2 - a2, 2))
}
function furthestSchool() {
    var nameA = document.getElementById('nameA').value;
    var nameB = document.getElementById('nameB').value;
    var nameC = document.getElementById('nameC').value;
    var xA = +document.getElementById('xA').value;
    var yA = +document.getElementById('yA').value;
    var xB = +document.getElementById('xB').value;
    var yB = +document.getElementById('yB').value;
    var xC = +document.getElementById('xC').value;
    var yC = +document.getElementById('yC').value;
    var xS = +document.getElementById('xS').value;
    var yS = +document.getElementById('yS').value;

    if ((distance(xA, yA, xS, yS) >= distance(xB, yB, xS, yS))
        && (distance(xA, yA, xS, yS) >= distance(xC, yC, xS, yS))) {
        document.getElementById('furthestSchool').innerHTML = ` Bạn ${nameA} nhà xa trường nhất`
    } else if ((distance(xB, yB, xS, yS) >= distance(xA, yA, xS, yS))
        && (distance(xB, yB, xS, yS) >= distance(xC, yC, xS, yS))) {
        document.getElementById('furthestSchool').innerHTML = ` Bạn ${nameB} nhà xa trường nhất`
    } else {
        document.getElementById('furthestSchool').innerHTML = ` Bạn ${nameC} nhà xa trường nhất`
    }
}
// end BT8
